#!/bin/bash
uname=$(uname)
adb=""
if [ $uname == 'Darwin' ]; then
	adb="/Users/ray/android-sdks"
else
	adb="/opt/android-sdk"
fi

if [ $1 == "android" ]; then
	if [ $# -eq 2 ]; then
		if [ $2 == "retry" ]; then
			$adb/platform-tools/adb -d install -r bin/target/android/Student\ Registration_signed.apk
		fi
	else
		rake clean:android
		sleep 3		
		rake device:android:production
		sleep 3
		$adb/platform-tools/adb -d install -r bin/target/android/Student\ Registration_signed.apk
	fi
elif [ $1 == "ios" ]; then
	if [ $# -eq 2 ]; then
		if [ $2 == "retry" ]; then
			fruitstrap -b bin/target/iOS/iphoneos6.1/Release/Student_Registration.app
		fi
	else 
			echo 'CLEANING'
	rake clean:iphone

	sleep 3

	echo 'RENAMING'
	sed -i '' -e's/Debug/Release/g' build.yml
	sed -i '' -e's/sdk: iphonesimulator6.1/sdk: iphoneos6.1/g' build.yml
	echo 'BUILDING'

	rake device:iphone:production

	sleep 3

	echo 'RENAMING'
	sed -i '' -e's/Release/Debug/g' build.yml
	sed -i '' -e's/sdk: iphoneos6.1/sdk: iphonesimulator6.1/g' build.yml

	sleep 3

	echo 'RUNNING LDID'
	./ldid -s bin/target/iOS/iphoneos6.1/Release/Student_Registration.app/rhorunner

	sleep 3
	echo 'INSTALLING'
	fruitstrap -b bin/target/iOS/iphoneos6.1/Release/Student_Registration.app

	echo 'DONE'
	fi
fi
