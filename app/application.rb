require 'rho/rhoapplication'

class AppApplication < Rho::RhoApplication
  def initialize
    # Tab items are loaded left->right, @tabs[0] is leftmost tab in the tab-bar
    # Super must be called *after* settings @tabs!
    #@tabs = nil
    
    @@current_student = nil
    @@tempregs = nil
    @@panel = nil
    
    #To remove default toolbar uncomment next line:
@@toolbar = nil
    
    super
    @default_menu = { 
      "Home" => "/app", 
      "Courses" => "/app/Course/listmethods",
      "Departments" => "/app/Department/",
      "Time Table" => "app/Settings/router?url=/app/TempReg/",
      "Registrations" => "app/Settings/router?url=/app/Registration/",
      "Account Info" => "app/Settings/router?url=/app/Student/edit"
    }

    # Uncomment to set sync notification callback to /app/Settings/sync_notify.
    # SyncEngine::set_objectnotify_url("/app/Settings/sync_notify")
    # SyncEngine.set_notification(-1, "/app/Settings/sync_notify", '')
  end
  
  def self.get_loginpopup
    popup = <<-eos
    <div data-role="popup" class="popup" id="popupLogin" data-theme="a" data-overlay-theme="a" data-transition="flow" data-position-to="window" data-history="false">
          <form id="loginform" action="/app/Student/asynchttp" method="POST">
            <div style="padding:10px 20px;">
              <h3>Please sign in</h3>
              <input type="text" name="email" id="email" placeholder="Email" data-theme="a" />
              <input type="password" name="password" id="password" placeholder="Password" data-theme="a" />
              <input type="hidden" id="previousurl" name="previousurl" />
              <div data-icon="null" data-iconpos="null" data-theme="a">
                <button type="submit" data-theme="a">Sign in</button>
              </div>
            </div>
          </form>
    </div>
    eos
    return popup
  end

	def self.get_panel
    get_tempregs
    paneltop = <<-eos
    <div data-role="panel" id="panel" data-position="right" data-position-fixed="true" data-display="reveal" data-theme="a"><div id="paneloverlay"></div>
    eos

    if @@current_student == nil
      panelcontent = <<-eos
        <input type="hidden" value="none" id="remoteid" />
        <h3>Please sign in</h3>
        <p>You won't be able to add courses until you sign in.</p>
        <a href="#" data-role="button" id="loginbutton">Login</a>
        <a href="/app/Student/new" data-role="button" id="registerbutton">Register</a>
      eos
    else
      panelcontent = <<-eos
        <input type="hidden" value="#{@@current_student.remote_id}" id="remoteid" />
        <ul id="panellist" data-role="listview" data-theme="a" data-divider-theme="a">
          <li id="studentname">#{@@current_student.firstname} #{@@current_student.lastname}</li>
          <li data-role="list-divider">Temporary Registrations<span id="tempregcount" class="ui-li-count"> #{@@tempregs.length}</span></li>
      eos
      @@tempregs.each do |tempreg|
          panelcontent += <<-eos
          <li class="ui-btn-icon-left" data-regtype="tempreg" data-icon="check" id="tempreg#{tempreg.object}">
            <a href="/app/Section/show?id=#{tempreg.section_id}">#{tempreg.title}</a>
          </li>
          eos
      end
      panelcontent += <<-eos
          <li id="registrationsli" data-role="list-divider">Registrations<span id="regcount" class="ui-li-count">0</span></li>
          <li id="loadingregs">Loading...</li>
					<li data-role="list-divider">Shortcuts</li>
          <li data-icon="home" class="ui-btn-icon-left"><a href="/app">Home</a></li>
          <li data-icon="custom-course" class="ui-btn-icon-left"><a href="/app/Course/listmethods">Courses</a></li>
          <li data-icon="custom-department" class="ui-btn-icon-left"><a href="/app/Department">Departments</a></li>
          <li data-icon="custom-clock" class="ui-btn-icon-left"><a href="/app/TempReg">Time Table</a></li>
          <li data-icon="star" class="ui-btn-icon-left"><a href="/app/Registration">Registrations</a></li> 
          <li data-role="list-divider">Account Information</li>
          <li data-icon="edit" data-iconpos="left" class="ui-btn-icon-left"><a href="/app/Student/edit">Manage</a></li>
          <li id="logoutbutton" data-icon="back" data-iconpos="left" class="ui-btn-icon-left"><a data-direction="reverse" href="/app/Student/logout">Logout</a></li>
          </ul>
      eos
    end

    @@panel = paneltop + panelcontent + "</div>"#= paneltop + panelcontent + "</div>"
  end
	
	def self.current_student
    @@current_student
  end
  
  def self.change_current_student(student)
      @@current_student = student
  end
  
  def self.get_tempregs
    if @@current_student == nil
      @@tempregs = nil
    else
      @@tempregs = TempReg.find(:all, :conditions => {'student_id' => @@current_student.object})
    end
  end
  
end
