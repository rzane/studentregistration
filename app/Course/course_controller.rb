require 'rho/rhocontroller'
require 'helpers/browser_helper'

class CourseController < Rho::RhoController
  include BrowserHelper

	def listmethods
		render :action => :listmethods #, :back => '/app'
	end

  # GET /Course
  def index
    #@courses = Course.find(:all)
    render :action => :index, :back => url_for(:action => :listmethods)
  end

  def byletter
    @letter = @params["firstletter"]
    render :action => :byletter
  end

  # GET /Course/{1}
  def show
    #$theid = @params['department_id']
    #$jscommand = 'showCourses(' + $theid + ');'
    #WebView.execute_js($jscommand)
    render :back => 'Department'
  end

  # GET /Course/new
  def new
    @course = Course.new
    render :action => :new, :back => url_for(:action => :index)
  end

  # GET /Course/{1}/edit
  def edit
    @course = Course.find(@params['id'])
    if @course
      render :action => :edit, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # POST /Course/create
  def create
    @course = Course.create(@params['course'])
    redirect :action => :index
  end

  # POST /Course/{1}/update
  def update
    @course = Course.find(@params['id'])
    @course.update_attributes(@params['course']) if @course
    redirect :action => :index
  end

  # POST /Course/{1}/delete
  def delete
    @course = Course.find(@params['id'])
    @course.destroy if @course
    redirect :action => :index  
  end
end
