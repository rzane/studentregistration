require 'rho/rhocontroller'
require 'helpers/browser_helper'

class RegistrationTimesController < Rho::RhoController
  include BrowserHelper

  # GET /RegistrationTimes
  def index
    @registrationtimeses = RegistrationTimes.find(:all)
    render :back => '/app'
  end

  # GET /RegistrationTimes/{1}
  def show
    @registrationtimes = RegistrationTimes.find(@params['id'])
    if @registrationtimes
      render :action => :show, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # GET /RegistrationTimes/new
  def new
    @registrationtimes = RegistrationTimes.new
    render :action => :new, :back => url_for(:action => :index)
  end

  # GET /RegistrationTimes/{1}/edit
  def edit
    @registrationtimes = RegistrationTimes.find(@params['id'])
    if @registrationtimes
      render :action => :edit, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # POST /RegistrationTimes/create
  def create
    @registrationtimes = RegistrationTimes.create(@params['registrationtimes'])
    redirect :action => :index
  end

  # POST /RegistrationTimes/{1}/update
  def update
    @registrationtimes = RegistrationTimes.find(@params['id'])
    @registrationtimes.update_attributes(@params['registrationtimes']) if @registrationtimes
    redirect :action => :index
  end

  # POST /RegistrationTimes/{1}/delete
  def delete
    @registrationtimes = RegistrationTimes.find(@params['id'])
    @registrationtimes.destroy if @registrationtimes
    redirect :action => :index  
  end
end
