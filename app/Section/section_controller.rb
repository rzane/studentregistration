require 'rho/rhocontroller'
require 'helpers/browser_helper'

class SectionController < Rho::RhoController
  include BrowserHelper

	@@dcback ||= '/app' #=> Back from index (course or department)
  @@back ||= '/app' #=> Back from show





# GET /Section
  def index
    theid = @params['course_id']
    if @params.has_key?('dcback')
      paramback = @params['dcback']
      @@dcback = paramback.slice(paramback.index("/app")..-1)
    end

    @dcback = @@dcback
  end

  # GET /Section/show?id=
  def show
    $theid = @params['id']
	  if @params.has_key?('back')
      paramback = @params['back']
      @@back = paramback.slice(paramback.index("/app")..-1)
		end
		@back = @@back
  end
  
  def page_back
    WebView.navigate_back
  end
  
  # GET /Section/new
  def new
    @section = Section.new
    render :action => :new, :back => url_for(:action => :index)
  end

  # GET /Section/{1}/edit
  def edit
    @section = Section.find(@params['id'])
    if @section
      render :action => :edit, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # POST /Section/create
  def create
    @section = Section.create(@params['section'])
    redirect :action => :index
  end

  # POST /Section/{1}/update
  def update
    @section = Section.find(@params['id'])
    @section.update_attributes(@params['section']) if @section
    redirect :action => :index
  end

  # POST /Section/{1}/delete
  def delete
    @section = Section.find(@params['id'])
    @section.destroy if @section
    redirect :action => :index  
  end
end
