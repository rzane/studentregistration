require 'rho/rhocontroller'
require 'helpers/browser_helper'

class TempRegController < Rho::RhoController
  include BrowserHelper
  
  def listall
    $alltempregs = TempReg.find(:all)
  end

  # GET /TempReg
  def index
    current_student = AppApplication.current_student
    @tempregs = TempReg.find(:all, :conditions => {'student_id' => current_student.object})
    render :back => '/app'
  end

  def viewresponse
    render :action => :viewresponse, :back => '/app' 
  end

  def register
    current_student = AppApplication.current_student
    if current_student
      registrations = TempReg.find(:all, :conditions => {'student_id' => current_student.object})
      if registrations.count >= 0
        regarray = Array.new
        registrations.each do |reg|
          sectionid = reg.section_id
          regarray.push(sectionid)
        end

        regstring = regarray.to_s

        Rho::AsyncHttp.post(
          :url => "http://prowebdesigns.org:3000/registrations/regtempregs",
          :body => "tempregs=#{regstring}&student_id=#{current_student.remote_id}",
          :callback => (url_for :action => :httpget_callback)
        )
      else
        @message = "<h3>Error</h3><p>You need to add courses before you can register!</p>"
      end
    else
        @message = "<h3>Error</h3><p>You need to sign in first!</p>"
    end
    
    render :action => :viewhttp
  end

  def httpget_callback
      current_student = AppApplication.current_student
      #TempReg.delete_all
      response = @params['body']['registrations']
      if response.length == 0
        $registered = 'none'
      elsif response.length == 1
        $registered = "{\"registrations\": [\""
        $registered += "#{response[0]['id']}"
        $registered += "\"]}"
        @tempreg = TempReg.find(:first, :conditions => {'student_id' => current_student.object, 'section_id' => response[0]['section_id']})
        @tempreg.destroy if @tempreg
      else
        sectionids = Array.new
        $registered = "{\"registrations\": ["
        @i=0
        response.each do |reg|
          sectionids.push(reg['section_id'])
          $registered += '"' + "#{reg['id']}" + '"'
          if @i < (response.length - 1)
            $registered += ","
          end
          @tempreg = TempReg.find(:first, :conditions => {'student_id' => current_student.object, 'section_id' => reg['section_id']})
          @tempreg.destroy if @tempreg
          @i +=1
        end
        $registered += "]}"
      end

      failed = @params['body']['failed']
      if failed.length == 0
        $failed = "none"
      elsif failed.length == 1
        $failed = "{\"failed\":[\"#{failed[0]}\"]}"
      else
        $failed = "{\"failed\":["
        @i=0
        failed.each do |fail|
          $failed += '"' + "#{fail}" + '"'
          if @i < (failed.length - 1)
            $failed += ","
          end
          @i +=1
        end
        $failed += "]}"
      end

      WebView.navigate( "#{url_for :controller => :Registration, :action => :index}?registered=true" )
  end

  # GET /TempReg/{1}
  def show
    @tempreg = TempReg.find(@params['id'])
    if @tempreg
      render :action => :show, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # GET /TempReg/new
  def new
    @tempreg = TempReg.new
    render :action => :new, :back => url_for(:action => :index)
  end

  # GET /TempReg/{1}/edit
  def edit
    @tempreg = TempReg.find(@params['id'])
    if @tempreg
      render :action => :edit, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # POST /TempReg/create
  def create
    if @params['section_id'] != nil
      studentid = AppApplication.current_student
      if studentid != nil
        
        @existingtempreg = TempReg.find(
          :first,
          :conditions => {'section_id' => @params['section_id'], 'student_id' => studentid.object}
        )
        
        if @existingtempreg.nil?
          @tempreg = TempReg.create(
            :CRN => @params['crn'],
            :course_id => @params['course_id'],
            :section_id => @params['section_id'],
            :subject => @params['subject'],
            :courseNum => @params['courseNum'],
            :title => @params['title'],
            :days => @params['days'],
            :startTime => @params['starttime'],
            :endTime => @params['endtime'],
            :sectionNum => @params['sectionNum'],
            :student_id => studentid.object
          )

          if @tempreg
            jsonstring = generatejson(@tempreg)
            render :string => jsonstring
          else
            render :string => '{"error": "Failed to Save"}'
          end
        else
          render :string => '{"error": "Already registered"}'
        end
      else
        render :string => '{"error": "No User"}'
      end
    else
      render :string => '{"error": "Null Section ID"}'
    end
  end

  def generatejson(tempreg)
    jsontempreg = '{
        "tempreg": {
          "object": %{object},
    			"student_id": %{student_id},
    			"section_id": %{section_id},
    			"course_id": %{course_id},
    			"crn": %{crn},
    			"subject": "%{subject}",
    			"title": "%{title}",
    			"courseNum": %{courseNum},
    			"sectionNum": %{sectionNum},
    			"days": "%{days}",
    			"startTime": "%{startTime}",
    			"endTime": "%{endTime}"
    		}
    }' % {object: tempreg.object, student_id: tempreg.student_id, section_id: tempreg.section_id, 
    course_id: tempreg.course_id, crn: tempreg.CRN, subject: tempreg.subject, 
    title: tempreg.title, courseNum: tempreg.courseNum, sectionNum: tempreg.sectionNum, 
    days: tempreg.days, startTime: tempreg.startTime, endTime: tempreg.endTime}
    
    return jsontempreg
  end

  # POST /TempReg/{1}/update
  def update
    @tempreg = TempReg.find(@params['id'])
    @tempreg.update_attributes(@params['tempreg']) if @tempreg
    redirect :action => :index
  end

  # POST /TempReg/{1}/delete
  def delete
    @tempreg = TempReg.find(@params['id'])
    @tempreg.destroy if @tempreg
    render :string => @params['id']
  end
end
