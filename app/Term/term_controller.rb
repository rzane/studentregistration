require 'rho/rhocontroller'
require 'helpers/browser_helper'

class TermController < Rho::RhoController
  include BrowserHelper

  # GET /Term
  def index
    @terms = Term.find(:all)
    render :back => '/app'
  end

  # GET /Term/{1}
  def show
    @term = Term.find(@params['id'])
    if @term
      render :action => :show, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # GET /Term/new
  def new
    @term = Term.new
    render :action => :new, :back => url_for(:action => :index)
  end

  # GET /Term/{1}/edit
  def edit
    @term = Term.find(@params['id'])
    if @term
      render :action => :edit, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # POST /Term/create
  def create
    @term = Term.create(@params['term'])
    redirect :action => :index
  end

  # POST /Term/{1}/update
  def update
    @term = Term.find(@params['id'])
    @term.update_attributes(@params['term']) if @term
    redirect :action => :index
  end

  # POST /Term/{1}/delete
  def delete
    @term = Term.find(@params['id'])
    @term.destroy if @term
    redirect :action => :index  
  end
end
