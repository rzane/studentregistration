require 'rho/rhocontroller'
require 'helpers/browser_helper'

class DepartmentController < Rho::RhoController
  include BrowserHelper

  # GET /Department
  def index
    @departments = Department.find(:all)
    render :back => url_for(:controller => :Course, :action => :listmethods)
  end

  # GET /Department/{1}
  def show
    @department = Department.find(@params['id'])
    if @department
      render :action => :show, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # GET /Department/new
  def new
    @department = Department.new
    render :action => :new, :back => url_for(:action => :index)
  end

  # GET /Department/{1}/edit
  def edit
    @department = Department.find(@params['id'])
    if @department
      render :action => :edit, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # POST /Department/create
  def create
    @department = Department.create(@params['department'])
    redirect :action => :index
  end

  # POST /Department/{1}/update
  def update
    @department = Department.find(@params['id'])
    @department.update_attributes(@params['department']) if @department
    redirect :action => :index
  end

  # POST /Department/{1}/delete
  def delete
    @department = Department.find(@params['id'])
    @department.destroy if @department
    redirect :action => :index  
  end
end
