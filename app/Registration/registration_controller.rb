require 'rho/rhocontroller'
require 'helpers/browser_helper'

class RegistrationController < Rho::RhoController
  include BrowserHelper

  # GET /Registration
  def index
    @registrations = Registration.find(:all)
    render :back => '/app'
  end

  # GET /Registration/{1}
  def show
    render :action => :show
  end

  # GET /Registration/new
  def new
    @registration = Registration.new
    render :action => :new, :back => url_for(:action => :index)
  end

  # GET /Registration/{1}/edit
  def edit
    @registration = Registration.find(@params['id'])
    if @registration
      render :action => :edit, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # POST /Registration/create
  def create
    @registration = Registration.create(@params['registration'])
    redirect :action => :index
  end

  # POST /Registration/{1}/update
  def update
    @registration = Registration.find(@params['id'])
    @registration.update_attributes(@params['registration']) if @registration
    redirect :action => :index
  end

  # POST /Registration/{1}/delete
  def delete
    @registration = Registration.find(@params['id'])
    @registration.destroy if @registration
    redirect :action => :index  
  end
end
