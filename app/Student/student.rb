# The model has already been created by the framework, and extends Rhom::RhomObject
# You can add more methods here
class Student
  include Rhom::PropertyBag

  # Uncomment the following line to enable sync with Student.
  # enable :sync

  #add model specific code here
  
  @@previousurl ||= '/app'
  
  def self.previous_url
    @@previousurl
  end
  
  def self.set_previous_url(previousurl)
    previousurl = previousurl.slice(previousurl.index("/app")..-1)
    previousurl = previousurl.split('&back')[0]
    previousurl = previousurl.split('&dcback')[0]

    if previousurl == '/app/index.erb'
      @@previousurl = '/app'
    else
      @@previousurl = previousurl
    end
  end
end
