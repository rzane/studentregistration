require 'rho/rhocontroller'
require 'helpers/browser_helper'

class StudentController < Rho::RhoController
  include BrowserHelper
  
  def notloggedin
    render :action => :notloggedin
  end

  def asynchttp
    if @params.has_key?('previousurl')
      prevurl = @params['previousurl']
      Student.set_previous_url(prevurl)
    end
    
    
		email = @params['email']
		password = @params['password']
		
    Rho::AsyncHttp.post(
      :url => "http://prowebdesigns.org:3000/sessions",
			:body => "email=#{email}&password=#{password}",
      :callback => (url_for :action => :httpget_callback)
    )
    redirect :action => :viewhttp
  end

  def httpget_callback
    email = @params['body']['email']
    if @params['body']['errors'] != nil || @params['body']['id'].to_s == ''
      jsstring = "failedLogin('" + Student.previous_url + "');"
      WebView.execute_js(jsstring)
    else
      @existingstudent = Student.find(:first, :conditions => {'email' => email})
  		if @existingstudent.nil?
        @student = createfromresp(@params['body'])
  		else
  		  @student = updatefromresp(@existingstudent, @params['body'])
  	  end
    
      AppApplication.change_current_student(@student)
      WebView.navigate(Student.previous_url)
    end
  end
  
  def createfromresp(response)
    student = Student.create(
    :email => response['email'],
	  :firstname => response['firstname'],
	  :lastname => response['lastname'],
	  :remote_id => response['id'], 
	  :regtime_id => response['regtime_id'], 
	  :studentidnumber => response['studentIDnumber'],
	  :searchable => response['searchable']
	  )
	  return student
  end
  
  def updatefromresp(student, response)
    student.update_attributes(
	    :email => response['email'],
		  :firstname => response['firstname'],
		  :lastname => response['lastname'],
		  :remote_id => response['id'], 
		  :regtime_id => response['regtime_id'], 
		  :studentidnumber => response['studentIDnumber'],
		  :searchable => response['searchable']
	  )
	  student.save
	  return student
  end

  def logout
    AppApplication.change_current_student(nil)
    redirect '/app'
  end

  # GET /Student
  def index
    @students = Student.find(:all)
    render :back => '/app'
  end

  # GET /Student/{1}
  def show
    @student = Student.find(@params['id'])
    if @student
      render :action => :show, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # GET /Student/new
  def new
    @student = Student.new
    render :action => :new, :back => url_for(:action => :index)
  end

  # GET /Student/{1}/edit
  def edit
    #@student = Student.find(@params['id'])
    #if @student
    #  render :action => :edit, :back => url_for(:action => :index)
    #else
    #  redirect :action => :index
    #end
  end

  def viewcreateresponse
    @redirect = @params['redirect']
    render :action => :createresponse, :back => '/app'
  end


  def register
    student = @params['student']
    #@message = student['firstname']

    json = <<-eos
    {
      "email": "#{student['email']}",
      "firstname": "#{student['firstname']}",
      "lastname": "#{student['lastname']}",
      "password": "#{student['password']}",
      "regtime_id": "#{student['type']}",
      "studentIDnumber": "#{student['studentidnumber']}"
    }
    eos
    @message = json
    Rho::AsyncHttp.post(
      :url => "http://prowebdesigns.org:3000/students",
      :body => "student=#{json}",
      :callback => (url_for :action => :create_callback)
    )
    render :action => :viewhttp
  end

  def create_callback
      response = @params['body']
      successful = handlecreateuserResponse(response)
      if successful == true
        $message = '<h3>User Successfully Created</h3><p>You are being redirected.</p>'
        params = '?redirect=true'
      else
        $message = '<h3>Error</h3><p>Please verify that your information is correct.</p>'
        params = '?redirect=false'
      end

      WebView.navigate( "#{url_for :action => :viewcreateresponse}#{params}" )
  end

  def handlecreateuserResponse(response)
    success = false
    id = response['id']
    idstring = id.to_s
    if idstring != ''
      success = true
    end
    return success
  end

  # POST /Student/{1}/update
  def update
    @student = Student.find(@params['id'])
    @student.update_attributes(@params['student']) if @student
    redirect :action => :index
  end

  # POST /Student/{1}/delete
  def delete
    @student = Student.find(@params['id'])
    @student.destroy if @student
    redirect :action => :index  
  end
end
