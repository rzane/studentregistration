require 'rho/rhocontroller'
require 'helpers/browser_helper'

class InstructorController < Rho::RhoController
  include BrowserHelper

  # GET /Instructor
  def index
    @instructors = Instructor.find(:all)
    render :back => '/app'
  end

  # GET /Instructor/{1}
  def show
    @instructor = Instructor.find(@params['id'])
    if @instructor
      render :action => :show, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # GET /Instructor/new
  def new
    @instructor = Instructor.new
    render :action => :new, :back => url_for(:action => :index)
  end

  # GET /Instructor/{1}/edit
  def edit
    @instructor = Instructor.find(@params['id'])
    if @instructor
      render :action => :edit, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # POST /Instructor/create
  def create
    @instructor = Instructor.create(@params['instructor'])
    redirect :action => :index
  end

  # POST /Instructor/{1}/update
  def update
    @instructor = Instructor.find(@params['id'])
    @instructor.update_attributes(@params['instructor']) if @instructor
    redirect :action => :index
  end

  # POST /Instructor/{1}/delete
  def delete
    @instructor = Instructor.find(@params['id'])
    @instructor.destroy if @instructor
    redirect :action => :index  
  end
end
