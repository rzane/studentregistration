/********** Application *************/
$(document).on('pageinit', function() {
	var panelopenedbefore = false;
	var ajax;
	var AppView = Backbone.View.extend({
		el: $('div[data-role="page"]'),
		events: {
			'click a#loginbutton': 'showLogin'
		},
		initialize: function() {
			console.log('App Init');
			this.preparePanel();
			this.prepareSwipe();
			this.prepareResize();
			this.prepBackFastClicks();
		},
		prepBackFastClicks: function() {
			console.log('fastclick prepped');
			$('a[data-direction="reverse"]').fastClick(function(e) {
				console.log('fastclick');
				e.preventDefault();
				var url = $(this).attr('href');
				var specifieddirection = $(this).data('direction');
				var reverse;
				if (specifieddirection == 'reverse') {
					reverse = true;
				} else {
					reverse = false;
				}

				$.mobile.changePage( url, {
				  transition: "slide",
				  reverse: reverse
				});
			});
		},
		preparePanel: function() {
			$(document).on('panelbeforeopen', function() {
				$('div.ui-page-active #paneloverlay').height($('div.ui-page-active #panel').height());
			});

			$(document).one('panelopen', function(event) {
				event.preventDefault();
				var studentid = $('div.ui-page-active input#remoteid').val();
				if (studentid != 'none' && panelopenedbefore == false) {
					application.loadRegistrations(studentid);
				}
				
				var currenturl = document.URL;
				var first = $('#panel .ui-li-divider:eq(0)');
				var last = $('#panel .ui-li-divider:eq(1)');
				var elements = first.nextUntil(last);
				elements.each(function() {
					if ($(this).find('a').text() != "") {
						var el = $(this).find('a');
						var href = el.attr('href');
						el.attr('href', href + "&back=" + currenturl);
					}
				});

			});
		},
		prepareResize: function() {
			$(window).on('resize, scroll', function() {
				$('div.ui-page-active #paneloverlay').height($('div.ui-page-active #panel').height());				
			});
		},
		prepareSwipe: function() {
			//$(document).on("swipeleft", "div.ui-page-active.swipepage", function() {
			//	$('div.ui-page-active #panel').panel('open');
			//});
			$(document).hammer().on("swipeleft", "div.ui-page-active.swipepage", function(event) {
				//$('div.ui-page-active #panel').panel('open');
				//console.log(event);
				var angle = Math.abs(event.gesture.angle);
				//console.log(angle);
				if (angle > 168) {
					console.log('good swipe: ' + angle);
					$('div.ui-page-active #panel').panel('open');
					//$('div.ui-page-active').html("<h3>Good Swipe<br>"+angle+"</h3>");
				} else {
					//$('div.ui-page-active').html("<h3>Bad Swipe<br>"+angle+"</h3>");
					console.log("bad swipe: " + angle);
				}
			 });

		},
		showLogin: function() {
			if ($('div.ui-page-active').hasClass('ui-page-panel-open')) {
				//panel is open
				var mypanel = $( "div.ui-page-active div#panel");
				$(document).one( "panelclose", mypanel, function( event, ui ) {
					console.log('panelclose');
					$.mobile.activePage.find('#popupLogin').popup('open');
					$('div.ui-page-active input#email').val('rzane@mix.wvu.edu');
					$('div.ui-page-active input#password').val('testpass');
					$('div.ui-page-active input#previousurl').val(document.URL);
				} );
				mypanel.panel("close");
				mypanel.unbind("panelclose");
			} else {
				//panel is closed
					$.mobile.activePage.find('#popupLogin').popup('open');
					$('div.ui-page-active input#email').val('rzane@mix.wvu.edu');
					$('div.ui-page-active input#password').val('testpass');
					$('div.ui-page-active input#previousurl').val(document.URL);
			}

		},
		loadRegistrations: function (studentid) {
			panelopenedbefore = true;

			console.log('LOADING REGS');
			$('div.ui-page-active li.registrationonpanel').remove();
			var url = "http://prowebdesigns.org:3000/registrations?student_id=" + studentid;
			ajax = $.getJSON(url, function(data) {
				$('div.ui-page-active li#loadingregs').remove();
				$('div.ui-page-active #regcount').text(data.length);
				if (data.length > 0) {
					for (var i = 0; i < data.length; i++) {

						var link = $('<a>').attr({'href':'/app/Registration/'}).text(data[i]['course']['title']);
						var item = $('<li>').append(link).attr({'data-icon':'star', 'data-regid':data[i]['id']}).addClass('ui-btn-icon-left registrationonpanel');
						$('div.ui-page-active ul#panellist li#registrationsli').after(item).parent().listview('refresh');
					}
				}
				
				application.checkforDuplicates();
			});

		},
		checkforDuplicates: function() {
			var liText = '';
			var liList = $('div.ui-page-active ul#panellist li.registrationonpanel');
			var listForRemove = [];
			console.log(liList);
			$(liList).each(function() {
				var text = $(this).data('regid');
			
				if (liText.indexOf('|'+ text + '|') == -1) {
					liText += '|'+ text + '|';
				}
				else {
					listForRemove.push($(this));
				}
			});

			$(listForRemove).each(function () {
				$(this).remove();
			});
		}
	});

	var application = new AppView;
});
