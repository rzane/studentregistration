/******** Section Show ********/
$(document).on('pageinit', '#sectionshow', function(e) {

	var Section = Backbone.Model.extend({});

	var SectionView = Backbone.View.extend({
		el: $('ul#sectionshowlist'),
		initialize: function() {
			console.log('initialize');
			this.beginLoad();

		},
		beginLoad: function() {
			$(document).one('pageshow', function() {
				$.mobile.loading('show');
				console.log('pageshow');
				var id = sectionview.getParameterByName("id", document.URL);
				sectionview.getSection(id);
			});
		},
		prepareClicks: function() {
			$(document).on('click', '#addsectionlink', _.debounce(function(e) {
					sectionview.addSection($(this));
			}, 1000, true));
			
			$(document).on('click', '#removesectionlink', _.debounce(function(e) {
				e.stopPropagation();
				sectionview.removeSection(e);
			}, 1000, true));
		},
		addSection: function(clicked) {
			
			$.post("/app/TempReg/create", {
				crn: clicked.data('crn'),
				course_id: clicked.data('course_id'),
				section_id: clicked.data('section_id'),
				subject: clicked.data('subject'),
				courseNum: clicked.data('coursenum'),
				title: clicked.data('title'),
				days: clicked.data('days'),
				starttime: clicked.data('starttime'),
				endtime: clicked.data('endtime'),
				sectionNum: clicked.data('sectionnum')
			}).done(function(data) {
				parsed = $.parseJSON(data);
				if (data.indexOf('error') >= 0) {
					console.log(parsed['error']);
					if (parsed['error'] == 'No User') {
						$('div.ui-page-active a#loginbutton').trigger('click');
					}
				} else {
					sectionview.addTempToPanel(parsed);
				}
			});
		},
		addTempToPanel: function(parsed) {
			var url = "/app/Section/show?id=" + parsed['tempreg']['section_id'];
		    var itemid = 'tempreg' + parsed['tempreg']['object'];
		    var item = $('<li>').css('left', '200px').attr({'id':itemid, 'data-icon':'check','data-regtype':'tempreg'}).addClass('ui-btn-icon-left').append( $('<a>').attr({'href':url, 'id':parsed['tempreg']['object']}).text( parsed['tempreg']['title'] ) );
				
				$('div.ui-page-active #panel').panel('open');
				var numberoftempregs = parseInt($('div.ui-page-active span#tempregcount').text());
				setTimeout(function() {
					if (numberoftempregs == 0) {
						$('div.ui-page-active ul#panellist li:eq(1)').after(item.animate({left:'0px', opacity:"show"}, 800));
						$('div.ui-page-active span#tempregcount').text(numberoftempregs + 1);
						$('div.ui-page-active ul#panellist').listview('refresh');
					}
					else {
						var itemheight = String($("div.ui-page-active ul#panellist li[data-regtype='tempreg']:eq(0)").outerHeight()) + 'px';
						$("div.ui-page-active ul#panellist li[data-regtype='tempreg']:eq(0)").animate({ marginTop: itemheight }, 500, function(){
							$("div.ui-page-active ul#panellist li:eq(1)").after(item.animate({left:'0px', opacity:"show"}, 800));
							$("div.ui-page-active ul#panellist li[data-regtype='tempreg']:eq(1)").css('marginTop', '0px');
							$('div.ui-page-active span#tempregcount').text(numberoftempregs + 1);
							$('div.ui-page-active ul#panellist').listview('refresh');
						});
					}
				    
				}, 100);
				
				$('div.ui-page-active div#addsectionbtn').hide();
				var rmbtn = $('<div>').attr({
					'id': 'removesectionlink',
					'data-role': 'button',
					'data-icon': 'minus',
					'data-tempregid': parsed['tempreg']['object']
				}).text('Remove');
				var newbutton = $('<div>').attr('id', 'removesectionbtn').append(rmbtn);
				$('div.ui-page-active div#buttoncontainer').append(newbutton);
				rmbtn.button();
		
		},
		removeSection: function(e){
			var clicked = $("div.ui-page-active div#removesectionlink");
			var deleteurl = "/app/TempReg/{" + clicked.data('tempregid') + "}/delete";
		    $.post(deleteurl).done(function(data) {
		      $('div.ui-page-active div#removesectionbtn').remove();
		      $('div.ui-page-active div#addsectionbtn').show();
		      var tmpregid = data.replace(/{/g, "").replace(/}/g, "").replace(/\./g, "\\.");
		      var selector = 'div.ui-page-active li#tempreg' + tmpregid;
		      $('div.ui-page-active #panel').panel('open');
  		      	$('div.ui-page-active span#tempregcount').text($("div.ui-page-active ul#panellist li[data-regtype='tempreg']").length - 1);

		      //$('div.ui-page-active span#tempregcount').text(parseInt($('div.ui-page-active span#tempregcount').text()) - 1);
		      $(selector).hide(800, function(){ $(selector).remove(); });
	    	});
		},
		getSection: function(id) {
			var section = new Section;
			console.log('id is :' + id);
			section.url = "http://prowebdesigns.org:3000/sections/" + id;
			section.fetch({
				success: function() {
					//var coursename = sections.first().get('course').title;
					//$('#coursename').text(coursename);
					sectionview.render(section);
					//$.mobile.loading('hide');
					$(this.$el).listview('refresh');
				}
			});
		},
		render: function(section) {
			var courseID = section.get('course').id;
			var sectionID = section.get('id');
			var crn = section.get('crn');
			var subject = section.get('course').subject;
			var courseNum = section.get('course').courseNum;
			var title = section.get('course').title;
			var days = section.get('days');
			var starttime = section.get('prettyStartTime');
			var endtime = section.get('prettyEndTime');
			var sectionNum = section.get('sectionNum');

			$('div.ui-page-active #addsectionlink').attr({
				'data-crn': crn,
				'data-course_id': courseID,
				'data-section_id': sectionID,
				'data-subject': subject,
				'data-courseNum': courseNum,
				'data-title': title,
				'data-days': days,
				'data-starttime': starttime,
				'data-endtime': endtime,
				'data-sectionNum': sectionNum
			});

			$('div.ui-page-active #crn').text(crn);
			$('div.ui-page-active #course').text(subject + " " + courseNum);
			$('div.ui-page-active #title').text(title);
			$('div.ui-page-active #days').text(days);
			$('div.ui-page-active #times').text(starttime + "-" + endtime);
			$('div.ui-page-active #instructorName').text(section.get('instructor').firstname + " " + section.get('instructor').lastname);
			$('div.ui-page-active #dept').text(section.get('department').name);
			$('div.ui-page-active #deptlink').attr('href', "/app/Course/show?department_id=" + section.get('department').id);
			$('div.ui-page-active #capacity').text(section.get('capacity'));
			$('div.ui-page-active #enrolled').text(section.get('enrolled'));
			var bufferzeros = "0";

			if (sectionNum.toString().length == 1) {
				bufferzeros += "0";
			}
			$('div.ui-page-active #sectionNum').text('Section ' + bufferzeros + sectionNum);
			
			this.prepareClicks();
			
			$.mobile.loading('hide');
		},
		getParameterByName: function(name, url) {
			var match = RegExp('[?&]' + name + '=([^&]*)').exec(url);
			return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
		}
	});

	var sectionview = new SectionView;

});