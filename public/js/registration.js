/*********** REGISTRATION **********/
$(document).on('pageinit', "#registration", function() {

	var repeater = null;
	var ajax;
	initialize();

	function getParameterByName(name, url) {
			var match = RegExp('[?&]' + name + '=([^&]*)').exec(url);
			return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
	}

	function initialize() {
		console.log('Registration init');
		beginLoad();
	}

	function beginLoad() {
		$(document).one('pageshow', "#registration", function() {
			$.mobile.loading('show');
			var studentid = $('div.ui-page-active #studentid').val();
			var regtimeid = $('div.ui-page-active #regtimeid').val();
			prepCountdown(regtimeid);
			loadRegistrations(studentid);

			prepRegistrationCheck();
		})
	}
	function prepRegistrationCheck() {
		var registered = getParameterByName('registered', document.URL);
		if (Boolean(registered)) {
			addNewRegsToList();
		}
		else {
			console.log('no regs');
		}
	}

	function addNewRegsToList() {
		var registrations = $('div.ui-page-active div#registrations').text();
		var failedregs = $('div.ui-page-active div#failedregistrations').text();
		alertFailedRegs(failedregs);
		console.log(registrations);
		getNewRegs(registrations);
	}

	function getNewRegs(registrations) {
		$('div.ui-page-active li.newreg').remove();
		ajax = $.ajax({
		  url: 'http://prowebdesigns.org:3000/sections/getfromregistrations',
		  type: 'POST',
		  data: { regs:registrations },
		  success: function(data) {
		  	for (var i = 0; i < data.length; i++) {
				renderReg(data[i]['id'], data[i]['course']['title'], true);
				removeDuplicateRegs();
		  	}
		  }
		});
	}

	function alertFailedRegs(failedregistrations) {

		var message = new Array;
		if (failedregistrations == 'none') {
			message.push('<b>Temporary Registrations successfully added</b>');
		} else {
			var failedjson = $.parseJSON(failedregistrations);
			var failed = failedjson['failed'];

			if (failed.length == 0) {
				message.push('<b>Temporary Registrations successfully added</b>');
			} else {
				message.push('<b> ');
				message.push(failed.length);
				message.push(' of your Temporary Registrations ');
				if (failed.length == 1) {
					message.push('was not successfully added:</b><br>');
				} else {
					message.push('were not successfully added:</b><br>');
				}
				message.push(failed.join('<br/>'));
			}
		}


		var paragraph = $('<p>').html(message.join(''));
		$('div.ui-page-active .ui-content').prepend(paragraph);
	}

	function removeDuplicateRegs() {
		var liText = '';
		var liList = $('div.ui-page-active ul#registrations li');
		var listForRemove = [];
		console.log(liList);
		$(liList).each(function() {
			var text = $(this).data('regid');
		
			if (liText.indexOf('|'+ text + '|') == -1) {
				liText += '|'+ text + '|';
			}
			else {
				listForRemove.push($(this));
			}
		});

		$(listForRemove).each(function () {
			$(this).remove();
		});
	}

	function prepCountdown(regtimeid) {
		if (regtimeid != 'no student') {
			var url = 'http://prowebdesigns.org:3000/regtimes/' + regtimeid;
			ajax = $.getJSON(url, function(data) {
				var endDate = data['prettyTime'];
				startCountdown(endDate);
			});
		}
		else {
			$('div.ui-content').hide();
			$('div.ui-page-active .ui-content').append('<h1>Please login</h1><p>You must login to view this page.</p>');
		}
	}
	function startCountdown(endDate) {
		console.log(endDate);
	    $('.countdown-styled').countdown({
	      date: endDate,
	      render: function(data) {
	        var el = $(this.el);
	        el.empty()
	          .append("<div>" + this.leadingZeros(data.days, 3) + " <span>days</span></div>")
	          .append("<div>" + this.leadingZeros(data.hours, 2) + " <span>hrs</span></div>")
	          .append("<div>" + this.leadingZeros(data.min, 2) + " <span>min</span></div>")
	          .append("<div>" + this.leadingZeros(data.sec, 2) + " <span>sec</span></div>");
	      }
	    });
	}

	function renderReg(id, title, newreg) {
		var url = "/app/Registration/show?regid=" + id + "&title=" + title;
		var link = $('<a>').attr('href',url).text(title);
		var item = $('<li>').append(link).attr('data-regid',id);



		console.log(id + "\t" + title + "\t" + newreg);
		if (newreg) {
			item.addClass('newreg');
		}
		$('div.ui-page-active ul#registrations').append(item).listview('refresh');
		$.mobile.loading('hide');
	}

	function loadRegistrations(studentid) {
		console.log('LOADING REGS');
		var url = "http://prowebdesigns.org:3000/registrations?student_id=" + studentid;
		ajax = $.getJSON(url, function(data) {
			console.log(data.length);
			$('div.ui-page-active ul#registrations').empty();

			if (data.length > 0) {
				for (var i = 0; i < data.length; i++) {
					renderReg(data[i]['id'], data[i]['course']['title'], false);
				}
				removeDuplicateRegs();
			} else {
				$('div.ui-page-active ul#registrations').append($('<h3>').text('No Registrations').css('text-align','center')).listview('refresh');
				$.mobile.loading('hide');
			}

		});

	}
});