var CourseModel = Backbone.Model.extend({
  url: function() {
    return "http://prowebdesigns.org:3000/courses.json";
  }
});

var DepartmentModel = Backbone.Model.extend({
  url: function() {
    return "http://prowebdesigns.org:3000/departments.json";
  }
});

