/******** Courses by Dept (Show) **********/
$(document).on('pageinit', "#showCourse", function(e) {

	var Course = Backbone.Model.extend({});
	var Courses = Backbone.Collection.extend({
		model: Course
	});

	var CoursesView = Backbone.View.extend({
		el: $('ul#courseshowlist'),
		initialize: function() {
			$('#showCourse').one('pageshow', function() {
				$.mobile.loading('show');
				var deptid = coursesview.getParameterByName("department_id", document.URL);
				coursesview.getCourses(deptid);
			});
		},
		getCourses: function(deptid) {
			$.mobile.loading('show');
			//var courses = new Courses;
			var url = "http://prowebdesigns.org:3000/courses/" + deptid;
			console.log('DEPT ID is ' + deptid);
			$.getJSON(url, function(data) {
				console.log(data);
				$('div.ui-page-active #showtitle').text(data['courses']['deptname']);
				$('ul#courseshowlist').append(data['courses']['htmlstring']).listview('refresh');
				coursesview.prepFastClick();
				$.mobile.loading('hide');
			});
		},
		prepFastClick: function() {
	      $('ul#courseshowlist a').fastClick(function(e) {
	        console.log('fastclick');
	        e.preventDefault();
	        var url = $(this).attr('href');
	        var specifieddirection = $(this).data('direction');
	        var reverse;
	        if (specifieddirection == 'reverse') {
	          reverse = true;
	        } else {
	          reverse = false;
	        }

	        $.mobile.changePage( url, {
	          transition: "slide",
	          reverse: reverse
	        });
	      });
	    },
		getParameterByName: function(name, url) {
			var match = RegExp('[?&]' + name + '=([^&]*)').exec(url);
			return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
		}
	});

	var coursesview = new CoursesView;

	var CourseView = Backbone.View.extend({
		tagName: 'li',
		render: function() {

			//var coursetitle = this.model.get('title');
			//var deptid = this.model.get('department_id');
			//var deptname = this.model.get('department').name;
			//var url = '/app/Section?course_id=' + this.model.get('id') + "&dcback=" + document.URL;
			//var link = $('<a>').text(coursetitle).attr('href', url);
			//return $(this.el).attr({
			//	'data-department-id': deptid,
			//	'data-department-name': deptname
			//}).append(link).addClass('courselistitem');
		}
	});
});