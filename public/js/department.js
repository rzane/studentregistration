/******** Department ***********/
$(document).on('pageinit', "#departments", function(e) {
	var ajax;
	var DepartmentsView = Backbone.View.extend({
		el: $('ul#deptlist'),
		initialize: function() {
			$.mobile.loading('show');
			this.getDepartments();
			$('div.ui-page-active a#backbutton').click(function() {
				ajax.abort();
			});
		},
		getDepartments: function() {
			
			var url = "http://prowebdesigns.org:3000/departments/lazyload";
			ajax = $.getJSON(url, function(data) {
				departmentsview.render(data);
			});

		},
		render: function(departments) {
			this.$el.append(departments['departments']['htmlstring']);
			this.$el.listview('refresh');
			this.prepFastClick();
			$.mobile.loading('hide');
		},
		prepFastClick: function() {
	      $('ul#deptlist a').fastClick(function(e) {
	        console.log('fastclick');
	        e.preventDefault();
	        var url = $(this).attr('href');
	        var specifieddirection = $(this).data('direction');
	        var reverse;
	        if (specifieddirection == 'reverse') {
	          reverse = true;
	        } else {
	          reverse = false;
	        }

	        $.mobile.changePage( url, {
	          transition: "slide",
	          reverse: reverse
	        });
	      });
	    }
	});

	var departmentsview = new DepartmentsView;

});
