/******** Section Index *********/
$(document).on('pageinit', '#listsections', function(e) {

	var Section = Backbone.Model.extend({});

	var Sections = Backbone.Collection.extend({
		model: Section
	});

	var SectionsView = Backbone.View.extend({
		el: $('ul#sectionlist'),
		initialize: function() {
			this.beginLoad();
			console.log('initialize!!!');
		},
		beginLoad: function() {
			$(document).one('pageshow', function() {
				$.mobile.loading('show');
				console.log('PAGESHOW');
				var course_id = sectionsview.getParameterByName("course_id", document.URL);
				sectionsview.getSections(course_id);
			});
		},
		getSections: function(course_id) {
			var sections = new Sections;
			sections.url = "http://prowebdesigns.org:3000/sections?course_id=" + course_id;
			sections.fetch({
				success: function() {
					sections.each(function(section) {
						sectionsview.render(section);
					});
				}
			});
		},
		render: function(section) {
			var view = new SectionView({
				model: section
			});
			var coursename = section.get('course').title;
			$('#coursename').text(coursename);
			this.$el.append(view.render());
			this.$el.listview('refresh');
			this.prepFastClick();
			$.mobile.loading('hide');
		},
		getParameterByName: function(name, url) {
			var match = RegExp('[?&]' + name + '=([^&]*)').exec(url);
			return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
		},
		prepFastClick: function() {
	      $('ul#sectionlist a').fastClick(function(e) {
	        console.log('fastclick');
	        e.preventDefault();
	        var url = $(this).attr('href');
	        var specifieddirection = $(this).data('direction');
	        var reverse;
	        if (specifieddirection == 'reverse') {
	          reverse = true;
	        } else {
	          reverse = false;
	        }

	        $.mobile.changePage( url, {
	          transition: "slide",
	          reverse: reverse
	        });
	      });
	    }
	});

	var sectionsview = new SectionsView;

	var SectionView = Backbone.View.extend({
		tagName: 'li',
		render: function() {
			var url = '/app/Section/show?id=' + this.model.get('id') + "&back=" + document.URL;
			var sectionNum = this.model.get('sectionNum');
			var instructorname = this.model.get('instructor').lastname + ", " + this.model.get('instructor').firstname;
			var days = this.model.get('days');
			var bufferzeros = "0";
			if (sectionNum.toString().length == 1) {
				bufferzeros += "0";
			}
			var listitemhead = $('<h3>').text(bufferzeros + sectionNum).addClass('sectionNumber');
			var instructorline = $('<h3>').text(instructorname);
			var times = this.model.get('prettyStartTime') + " - " + this.model.get('prettyEndTime');
			var listitempara = $('<p>').html(days + " " + times);
			var link = $('<a>').attr('href', url).append(listitemhead).append(instructorline).append(listitempara);

			return $(this.el).append(link).addClass('sectionlistitem');
		}
	});
});