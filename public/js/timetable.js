$(document).on('pageshow', '#timetable', function(e) {
	initialize();
	addIcons();
	console.log('timetable');
	$('li.time').each(function() {
		$(this).outerHeight(38);
	});
});

function prepareClicks() {
	$('div.ui-page-active div.classonlist').unbind();
	$('div.ui-page-active div.classonlist').click(function (e) {
		//console.log('clicked');
		$('#popupcoursetimeinfo').empty();
		var clicked = $(this);

		var days = clicked.data('days');
		var starttime = clicked.data('starttime');
		var endtime = clicked.data('endtime');
		var sectionid = clicked.data('sectionid');

		if (clicked.hasClass('registration')) {
			url = "/app/Registration"
			$('div.ui-page-active #viewsectionbutton span.ui-btn-text').text('View Registration');
		} else {
			url = '/app/Section/show?id=' + sectionid + "&back=" + document.URL;
			$('div.ui-page-active #viewsectionbutton span.ui-btn-text').text('View Section');
		}

		var coursetitle = $('<h3>').text(clicked.text());
		var timeinfo = $('<p>').html(days + "<br>" + starttime + " - " + endtime);
		$('#popupcoursetimeinfo').append(coursetitle).append(timeinfo);

		$('#viewsectionbutton').attr('href', url);
		$('#popupViewSection').popup('open');
	});

	$('#backbuttononpopup').unbind('click').click(function() {
		$('#popupViewSection').popup('close');
	});
};

function addIcons() {
	var buttonhtml = '<span class="ui-icon ui-icon-arrow-l ui-icon-shadow" style="right:30px;">&nbsp;</span>';
	$('.goleft').each( function() {
		console.log($(this));
		$(this).find('.ui-btn-inner').append(buttonhtml);
	});
}

function initialize() {
	$('div.ui-page-active ul.times').listview('refresh');
	getCourses();
	var mySwiper = $('.swiper-container').swiper();
	prepareToggle();
	prepareResize();
}

function prepareResize() {
	$(window).on("orientationchange",function() {
		console.log('orientationchange');
		setTimeout(function() {
			$('.classonlist').width($('div.timelinediv').width() - 10);
		}, 1000);
	});
}

function prepareToggle() {
	$( "div.ui-page-active #regtoggle" ).bind( "change", function(event, ui) {
		var value = $(this).val();
		if (value == 'reg') {
			$('div.ui-page-active .clone').remove()
			$('div.ui-page-active .classonlist').each(function() {
				$(this).appendTo('div.ui-page-active #removedcourses').removeAttr( 'style' );
			});
			getRegistrations();
		} else {
			console.log('tempreg');
			$('div.ui-page-active div.registration').remove();
			$('div.ui-page-active #removedcourses .classonlist').each(function(){
				$(this).appendTo('div.ui-page-active #courseholder');
			});
			getCourses();
		}
	});
}

function getRegistrations() {
	studentid = $('div.ui-page-active #studentid').val();
	var url = "http://prowebdesigns.org:3000/registrations?student_id=" + studentid

	$.getJSON(url, function(data) {
		//$('div.ui-page-active div#removedcourses').empty();
		$('div.ui-page-active div.registration.classonlist').remove();
		for (var i = 0; i < data.length; i++) {

			var para = $('<p>').addClass('coursepara').text(data[i]['course']['title']);

			var div = $('<div>').addClass('registration classonlist').attr({ 
				'id':'tempreg' + data[i]['section']['id'],
				'data-days':data[i]['section']['days'],
				'data-starttime':data[i]['section']['prettyStartTime'],
				'data-endtime':data[i]['section']['prettyEndTime'],
				'data-sectionid':data[i]['section']['id']
			}).append(para);			
			console.log(data);
			$('div.ui-page-active #courseholder').append(div);
		
		}
		getCourses();
	});
}

function getCourses() {
	$('#courseholder .classonlist').each(function( index ) {
		var days = $(this).data('days');
		days = days.replace(/\s+/g, '');
		var startTime = $(this).data('starttime');
		var endTime = $(this).data('endtime');
		console.log('START: ' + startTime + "\tEnd: " + endTime);
		var id = $(this).attr('id');
		alterCourse(id, days, startTime, endTime);
	});
	prepareClicks();
}

function alterCourse(id, days, startTime, endTime) {
	var startarray = splitTime(startTime);
	var endarray = splitTime(endTime);
	var courseheight = calcCourseHeight(startarray, endarray);
	var distfromtop = calcDistFromTop(startarray);
	var marginleft = '95px';
	var width = $('div.timelinediv').width() - 10;
	console.log(id + ": " + courseheight + "\t" + distfromtop);
	
	$('#' + id).css({
		'height':courseheight + 'px',
		'width':width,
		'top':distfromtop,
		'margin-left': marginleft
	});
	moveCourse(id, days);
}

function moveCourse(id, days){
	var daysarray = days.split("");
	for (var i = 0; i < daysarray.length; i++) {
		var fullday = getFullDay(daysarray[i]);
		var movetoselector = "#" + fullday + " " + "div.collapse";
		if (i == 0) {
			$("#" + id).appendTo(movetoselector);
		}
		else {
			$("#" + id).clone().appendTo(movetoselector).addClass('clone');
		}
	}
}


function getFullDay(day) {
	switch (day) { 
	    case 'M': 
	        return 'monday';
	        break;
	    case 'T': 
	        return 'tuesday';
	        break;
	    case 'W': 
	        return 'wednesday';
	        break;      
	    case 'R': 
	        return 'thursday';
	        break;
			case 'F': 
			    return 'friday';
			    break;
	    default:
	        console.log('Not a valid day');
	}
}

function splitTime(time) {
	var timearray = [];
	console.log(time);
	if (time == "TBA") {
		console.log('TIME WAS TBA');
	}
	else {
			var split1 = time.split(":");
	var split2 = split1[1].match(/.{1,2}/g);
	var hours = split1[0];
	var minutes = split2[0];
	var period = split2[1];
	var comparehours = parseInt(hours);
	if (period == 'am' && comparehours == 12) {
		hours = 0;
		console.log('Hours are 12: ' + hours + " " + period);
	}
	else if (period == 'pm' && comparehours == 12) {
		hours = comparehours;
		console.log('Hours are 12: ' + hours + " " + period);
	}
	else if (period == 'am' && comparehours >= 1 && comparehours <= 11) {
		hours = comparehours;
		console.log('AM - Hours are between 1 and 11: ' + hours + " " + period);
	}
	else if (period == 'pm' && comparehours >= 1 && comparehours <= 11) {
		hours = comparehours + 12;
		console.log('PM - Hours are between 1 and 11: ' + hours + " " + period);
	}
	else {
		console.log('No match: ' + hours + " " + period);
	}
	
	timearray.push(hours);
	timearray.push(parseInt(minutes));
	
	}

	return timearray;
}

function calcDistFromTop(startarray) {
	var halfhourpixels = $('li.time:first').outerHeight();
	var headerheight = $('.ui-collapsible-heading:first').outerHeight();
	
	var listingstarttime = $('li.time:first').text();
	var listingstartarray = splitTime(listingstarttime);
	
	var hourdiff = (startarray[0] - listingstartarray[0])*60;
	var mindiff = startarray[1] - listingstartarray[1];
	var timedist = hourdiff + mindiff;
	var baseline = (halfhourpixels/2) + headerheight;
	var topvalue = ((timedist/30) * halfhourpixels) + baseline;
	
	return topvalue;
}

function calcCourseHeight(startarray, endarray) {
	var halfhourpixels = $('li.time:first').outerHeight();
	console.log(startarray);
	console.log(endarray);
	var hourdiff = (endarray[0] - startarray[0])*60;
	var mindiff = endarray[1] - startarray[1];
	var timedist = hourdiff + mindiff;
	var courseheight = (timedist/30) * halfhourpixels;
	return courseheight;
}