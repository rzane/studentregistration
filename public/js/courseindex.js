/********** Course Index ***********/
$(document).on('pageinit', "#courses", function(e) {

	$.mobile.loading('show');

	console.log('courses index');
	var Course = Backbone.Model.extend({});
	var Courses = Backbone.Collection.extend({
		model: Course
	});
 
	var ajaxLoading = false;
	var performingSearch = false;
	var CoursesView = Backbone.View.extend({
		el: $('ul#courselist'),
		initialize: function() {
			console.log("CI PAGE INIT");
			this.getFirstCoursesPage();
			this.prepareScroll();
			this.prepareSearch();
			this.prepBack();
		},
		prepBack: function() {
		},
		getFirstCoursesPage: function() {
			$('#courses').one('pageshow', function() {
				coursesview.getCourses(1);
			});
		},
		reinitialize: function() {
			ajaxLoading = false;
			this.$el.empty().listview('refresh');
			$('div.ui-page-active li[data-role="list-divider"]').show();
			this.getCourses(1);
		},
		prepareSearch: function() {
			$(':text').typing({
				start: function(event, $elem) {
					console.log('typing start');
				},
				stop: function(event, $elem) {
					coursesview.performSearch($elem.val());
				},
				delay: 1000
			});

			$(document).on('click', 'div.ui-page-active a.ui-input-clear span.ui-btn-inner', function(){
				coursesview.reinitialize();
			});
		},
		performSearch: function(query) {
			if (query == "") {
				console.log('null query');
				this.reinitialize();
			}
			else if (performingSearch) {
				console.log('already performing search!');
				return false;
			}
			else {
				performingSearch = true;
				ajaxLoading = true;
				$('div.ui-page-active ul#courseslist').empty();
				var url = "http://prowebdesigns.org:3000/courses/search?query=" + query;
				console.log(url);
				$.getJSON(url, function(data) {
					coursesview.render(data);
					$('div.ui-page-active li[data-role="list-divider"]').hide();
					$.mobile.loading('hide');
					performingSearch = false;
					coursesview.checkforDuplicates();
				});
			}
		},
		checkforDuplicates: function() {
		var liText = '';
		var liList = $('div.ui-page-active ul#courseslist li');
		var listForRemove = [];
		$(liList).each(function() {
			var text = $(this).text();
		
			if (liText.indexOf('|'+ text + '|') == -1) {
				liText += '|'+ text + '|';
			}
			else {
				listForRemove.push($(this));
			}
		});

		$(listForRemove).each(function () {
			$(this).remove();
		});
		},
		getCourses: function(page) {
			console.log(page);

			if (ajaxLoading) {
				return false;
			} else {
				ajaxLoading = true;
				var url = "http://prowebdesigns.org:3000/courses/lazyload?page=" + page;
				$.getJSON(url, function(data) {
					console.log('CI GET COURSES');
					coursesview.render(data);
					$.mobile.loading('hide');
					ajaxLoading = false;
				});
			}
		},
		render: function(courses) {
			this.$el.append(courses['courses']['htmlstring']);
			this.$el.listview({
				autodividers: true,
				autodividersSelector: function(li) {
					var out = li.attr('data-department-name');
					return out;
				}
			});
			this.$el.listview('refresh');
		},
		prepareScroll: function() {

			var page = 1;
			$(window).scroll(function(){
				if ($(window).scrollTop() > 300) {
					if ($(window).scrollTop() + $(window).height() > $('div.ui-page-active#courses').height() - ($(window).height() - 10)) {
						if (ajaxLoading == false) {
							page += 1;
							coursesview.getCourses(page);
						}
						else {
							console.log('ajax content is Loading');
						}
					}
				}
			});
		}
	});

	var coursesview = new CoursesView;
});
