#!/bin/bash
if [ $1 == 'retry'  ]; then

fruitstrap -b bin/target/iOS/iphoneos6.1/Release/Student_Registration.app


else

echo 'CLEANING'
rake clean:iphone

sleep 3

echo 'RENAMING'
sed -i '' -e's/Debug/Release/g' build.yml
sed -i '' -e's/sdk: iphonesimulator6.1/sdk: iphoneos6.1/g' build.yml

echo 'BUILDING'

rake device:iphone:production

sleep 3

echo 'RENAMING'
sed -i '' -e's/Release/Debug/g' build.yml
sed -i '' -e's/sdk: iphoneos6.1/sdk: iphonesimulator6.1/g' build.yml

sleep 3

echo 'RUNNING LDID'
./ldid -s bin/target/iOS/iphoneos6.1/Release/Student_Registration.app/rhorunner


sleep 3
echo 'INSTALLING'
fruitstrap -b bin/target/iOS/iphoneos6.1/Release/Student_Registration.app

echo 'DONE'
fi
