#!/bin/bash

declare -a files=("application.js" "timetable.js" "courseindex.js" "department.js" "registration.js" "sectionshow.js" "coursesbydept.js" "login.js" "sectionindex.js")

sedcmd='unknown'
closurecmd='unknown'
unamestr=$(uname)
if [ $unamestr == 'Darwin' ]; then
	sedcmd='gsed'
	closurecmd='closure-compiler'
else
	sedcmd='sed'
	closurecmd='closure'
fi


if [ $1 == "prod" ]; then
	echo "Switching to prod..."
	loopto=$(( ${#files[@]} - 1  ))

	for i in $(seq 0 ${loopto}); do


		file=${files[$i]}
		delfile=$(echo ${files[$i]} | sed -e's/\./.del./g')

		dir="public/js/"
		erb=${locations[$i]}
		echo "Removing console.log's from $file"
		grep -v console ${dir}${file} > ${dir}${delfile}
	done

  	$sedcmd -i -e'/#@@toolbar\ =\ nil/c\@@toolbar\ =\ nil' app/application.rb
	cat public/js/*.del.* > public/js/stdregdel.js
	echo "Compiling..."
	$closurecmd --js public/js/stdregdel.js --js_output_file public/js/studentreg.js --compilation_level SIMPLE_OPTIMIZATIONS --externs public/js/externs/backbone-0.9.10-externs.js --externs public/js/externs/underscore-1.4.4.js
	
	echo 'Removing Temps...'
	rm -rf public/js/*.del.*
	rm -rf public/js/stdregdel.js

	echo 'Modifying layout.erb'
	$sedcmd -i -e'/<!--DEV-->/c\<!--DEV' app/layout.erb
	$sedcmd -i -e'/<!--EODEV-->/c\EODEV-->' app/layout.erb

	$sedcmd -i -e'/<!--PROD/c\<!--PROD-->' app/layout.erb
	$sedcmd -i -e'/EOPROD-->/c\<!--EOPROD-->' app/layout.erb

else
  echo "Switching to dev..."
	echo 'Modifying layout.erb'
	$sedcmd -i -e'/<!--PROD-->/c\<!--PROD' app/layout.erb
	$sedcmd -i -e'/<!--EOPROD-->/c\EOPROD-->' app/layout.erb

	$sedcmd -i -e'/<!--DEV/c\<!--DEV-->' app/layout.erb
	$sedcmd -i -e'/EODEV-->/c\<!--EODEV-->' app/layout.erb

  	$sedcmd -i -e'/@@toolbar\ =\ nil/c\#@@toolbar\ =\ nil' app/application.rb
fi


